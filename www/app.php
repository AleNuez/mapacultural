<?php 
require_once "config.php";
session_start();


// Hago un flag para identificar la primera vez que se se selecciona el nombre y lo subo a sesión para usarlo luego.

if(isset($_POST['provincia'])){
    $provincia_count = 1;
} else {
    $provincia_count = 0;
}
if($provincia_count == 1){
    $provincia_elegida = $_POST['provincia'];
    $_SESSION['provincia'] = $provincia_elegida;
    $provincia_count++;
}

if(isset($_POST['municipio'])){
    $municipio_count = 1;
} else {
    $municipio_count = 0;
}
if($municipio_count == 1){
    $municipio_elegido = $_POST['municipio'];
    $_SESSION['municipio'] = $municipio_elegido;

    if($provincia_elegida == "30" || "78" || "86") {
        $sql_f = "SELECT * FROM localidades where departamento_id = ".$municipio_elegido."";
    }else {
        $sql_f = "SELECT * FROM localidades where municipio_id = ".$municipio_elegido."";
    }

    
    if($result_f = mysqli_query($link, $sql_f)){
        if(mysqli_num_rows($result_f) > 0){
                while($row_f = mysqli_fetch_array($result_f)){
                    if($provincia_elegida == "30" || "78" || "86") {
                        $localidades = $row_f['departamento_id']; 
                    }else {
                        $localidades = $row_f['id'];  
                    }
                      
                }
            }
        }
    $_SESSION['localidades'] = $localidades;
    $municipio_count++;
}

// if(isset($_POST['localidad'])){
//     $localidad_count = 1;
// } else {
//     $localidad_count = 0;
// }
// if($localidad_count == 1){
//     $localidad_elegida = $_POST['localidad'];
//     $_SESSION['localidad'] = $localidad_elegida;
//     $localidad_count++;
// }

?>
<html >
    <head>
        <title>Encontrá Cultura</title>

        <!-- lang="es" <meta charset="utf-8"> -->
      
        <link
            rel="stylesheet"
            href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script
            src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    </head>
    <body>
        <div class="container">
            <?php echo "<h1>Mapa Cultural Argentino</h1>"; ?>
            <h2>Encontrá tu próxima salida</h2>

            <form action="reset.php" method="post">
                <button type="submit">Restablecer</button>
            </form>
           
            <div class="selector">

                <div class="row">
 <div class="col-md-4">
<?php 
if ($municipio_count == 0){
?>
                      <form action="app.php" method="post">
    <label class="col-sm-2 control-label">Provincia</label>
    <select name="provincia" id="selector-provincia" class="form-control">
    <?php // Cargar el primer Select
      
    
                         if($provincia_elegida != "" ){
                          $sql_a = "SELECT * FROM provincias where id = ".$provincia_elegida."";
                          if($result_a = mysqli_query($link, $sql_a)){
                           if(mysqli_num_rows($result_a) > 0){
                           while($row_a = mysqli_fetch_array($result_a)){
                           $nombre_provincia = $row_a['nombre'];  
                           $_SESSION['nombre_prov'] = $nombre_provincia;   
                                  }
                                 }
                                   }
                            echo "<option value=".$provincia_elegida." selected >".$nombre_provincia."</option>";
                           } else {

                             // mysqli_set_charset($link, 'utf8');
                    $sql = "SELECT * FROM provincias ORDER BY nombre ASC";
                    if($result = mysqli_query($link, $sql)){
                        if(mysqli_num_rows($result) > 0){
                                while($row = mysqli_fetch_array($result)){
                                        echo "<option value=".$row['id'].">".utf8_decode($row['nombre'])."</option>";
                                       
                                }
                        } else{
                            echo '<option>Sin Provincias</option>';
                        }
                         } else{
                        echo "Fatal Select.";
                          }
                      }
                    ?>
            </select>
            <button type="submit">>> Ver Municipios</button>
            </form>
</div>
<?php
}
?>
<!-- termina div provincia -->
<div class="col-md-4" id="div-municipio">

                        <?php
if ($municipio_count == 0){
                        if ($provincia_elegida == "2"){
                            $loca = "2000010";
                             }
          if($provincia_elegida != "" || $municipio_elegido != ""){
                         if($loca != "2000010" ){
?>
                        <form action="app.php" method="post">
                            <label class="col-sm-2 control-label">Departamento</label>
                            <select name="municipio" id="selector-municipio" class="form-control">
                            <?php
           
           if($municipio_elegido != ""){
            if($provincia_elegida == "30" || "78" || "86") {
                $sql_b = "SELECT * FROM departamentos where provincia_id = ".$provincia_elegida."";
            }else {
                $sql_b = "SELECT * FROM municipios where id = ".$municipio_elegido."";
            }
            
            if($result_b = mysqli_query($link, $sql_b)){
                if(mysqli_num_rows($result_b) > 0){
                        while($row_b = mysqli_fetch_array($result_b)){
                           $nombre_municipio = $row_b['nombre'];  
                           $_SESSION['nombre_municipio'] = $nombre_municipio;   
                        }
                    }
                }
            echo "<option value=".$municipio_elegido.">".$_SESSION['nombre_municipio']."</option>";

           
        } else {

            if($provincia_elegida == "30" || "78" || "86") {
                $sql2 = "SELECT * FROM departamentos where provincia_id = "."$provincia_elegida"." ORDER BY nombre ASC";
            }else {
                $sql2 = "SELECT * FROM municipios where provincia_id = "."$provincia_elegida"." ORDER BY nombre ASC";
            }
            
            if($result2 = mysqli_query($link, $sql2)){
                if(mysqli_num_rows($result2) > 0){
                        while($row2 = mysqli_fetch_array($result2)){
                                echo "<option value=".$row2['id'].">".utf8_decode($row2['nombre'])."</option>";
                        }
                } else{
                    echo '<option>Sin Municipios</option>';
                }
            } else{
                echo "Fatal Select.";
            }
        }
            ?>
                        </select>
                            <button type="submit">>> Ver Ofertas Culturales</button>
                        </form>
    <?php
}   

}
    ?>

            </div>
            <?php 
}
?>
<!-- termina div municipio -->
<div class="col-md-4" id="div-localidad">

</div><!-- termina div localidad -->
</div><!-- del row -->
<div id="div-tipo-oferta">


                </div><!-- del 12 -->
            </div><!-- del selector -->
        </div><!-- del container -->
        <div class="row">
            <div class="col-md-12">
            <?php
    // Control
    //   echo "<p>".$_POST['selector-tipo']."</p>";
    //   echo "<p>provincia count:</p>";
    //   echo "<p>".$provincia_count."</p>";
    //   echo "<p>provincia por session:</p>";
    //   echo "<p>".var_dump($_SESSION['provincia'])."</p>";
    //   echo "<p>provincia name:</p>";
    //   echo "<p>".var_dump($provincia_name)."</p>";
    //   echo "<p>municipio por session:</p>";
    //   echo "<p>".var_dump($_SESSION['municipio'])."</p>";
    //   echo "<p>municipio name:</p>";
    //   echo "<p>".var_dump($municipio_name)."</p>";
    //   echo "<p>localidad por session:</p>";
     
    //   echo "<p>localidad name:</p>";
    //   echo "<p>".var_dump($localidad_name)."</p>";

    
      ?>

    <div class="container">
                    <?php     
                     
                    // Attempt select query execution
                    $tipo = $_POST['selector-tipo'];
                    if($loca != "2000010"){
                        $loca = $_SESSION['localidades'];
                        $loca = substr($loca,0,7);
                    }
                    $muni = $_SESSION['municipio'];
                    $prov = $_SESSION['provincia'];


    if($loca != ""){ 
        if($prov == 94 ) {
            $loca = substr($loca,0,3);
        }

    
                    echo "<h2>Resultado de búsqueda</h2>";
                    echo "<h3>".$_SESSION['nombre_prov']."</h3>";
                   // echo "<p>".var_dump($loca)."</p>";
   //----------- Buscar Cines ----------
   if($provincia_elegida == "30" || "78" || "86"){
    $sql_busq = "SELECT * FROM cines WHERE dpto_id = '"."$loca"."'";
   }else{
    $sql_busq = "SELECT * FROM cines WHERE cod_loc LIKE '%"."$loca"."%'";
   }
                  
                    if($result_busq = mysqli_query($link, $sql_busq)){
                        if(mysqli_num_rows($result_busq) > 0){
                            echo "<br><h3>Cines</h3>";
                            echo '<table class="table table-bordered table-dark">';
                                echo "<thead>";
                                    echo "<tr>";
                                        echo "<th>Nombre</th>";
                                        echo "<th>Dirección</th>";
                                        echo "<th>Pantallas</th>";
                                        echo "<th>Butacas</th>";
                                        echo "<th>Teléfono</th>";
                                        echo "<th>Mail</th>";
                                        echo "<th>Web</th>";
                                    echo "</tr>";
                                echo "</thead>";
                                echo "<tbody>";
                                while($row_busq = mysqli_fetch_array($result_busq)){
                                    echo "<tr>";
                                        echo "<td>" . $row_busq['nombre'] . "</td>";
                                        echo "<td>" . $row_busq['direccion'] . "</td>";
                                        echo "<td>" . $row_busq['pantallas'] . "</td>";
                                        echo "<td>" . $row_busq['butacas'] . "</td>";
                                        echo "<td>" . $row_busq['telefono'] . "</td>";
                                         echo "<td>" . $row_busq['email'] . "</td>";
                                         echo "<td>" . $row_busq['website'] . "</td>";
                                    echo "</tr>";
                                }
                                echo "</tbody>";                            
                            echo "</table>";
                            // Free result set
                            mysqli_free_result($result_busq);
                        } else{    
                          //  echo '<div class="alert alert-danger"><em>No se encontraron ofertas en la zona.</em></div>';   
                        }
                    } else{
                       // echo "Seleccione la localidad para ver los resultados.";
                    }
                    // Close connection
                  //  mysqli_close($link);
    // ------- termina buscar Cines
    //----------- Buscar Monumentos  ----------
       $sql_monu = "SELECT * FROM monumentos WHERE cod_loc LIKE '%"."$loca"."%'";
       if($result_monu = mysqli_query($link, $sql_monu)){
           if(mysqli_num_rows($result_monu) > 0){
               echo "<br><h3>Monumentos y Sitios Históricos</h3>";
               echo '<table class="table table-bordered table-dark">';
                   echo "<thead>";
                       echo "<tr>";
                           echo "<th>Nombre</th>";
                           echo "<th>Dirección</th>";
                           echo "<th>Denominación específica</th>";
                           echo "<th>Descripción</th>";
                           echo "<th>Inauguración</th>";
                       echo "</tr>";
                   echo "</thead>";
                   echo "<tbody>";
                   while($row_monu = mysqli_fetch_array($result_monu)){
                       echo "<tr>";
                           echo "<td>" . $row_monu['nombre'] . "</td>";
                           echo "<td>" . $row_monu['direccion'] . "</td>";
                           echo "<td>" . $row_monu['denominacion_especifica'] . "</td>";
                           echo "<td>" . $row_monu['descripcion'] . "</td>";
                           echo "<td>" . $row_monu['fecha_de_inauguracion'] . "</td>";
                       echo "</tr>";
                   }
                   echo "</tbody>";                            
               echo "</table>";
               // Free result set
               mysqli_free_result($result_monu);
           } else{    
             //  echo '<div class="alert alert-danger"><em>No se encontraron ofertas en la zona.</em></div>';   
           }
       } else{
          // echo "Seleccione la localidad para ver los resultados.";
       }
       // Close connection
       //mysqli_close($link);
// ------- termina buscar monumentos
 //----------- Buscar Museos  ----------
 $sql_monu = "SELECT * FROM museos WHERE cod_loc LIKE '%"."$loca"."%'";
 if($result_monu = mysqli_query($link, $sql_monu)){
     if(mysqli_num_rows($result_monu) > 0){
         echo "<br><h3>Museos</h3>";
         echo '<table class="table table-bordered table-dark">';
             echo "<thead>";
                 echo "<tr>";
                     echo "<th>Nombre</th>";
                     echo "<th>Dirección</th>";
                     echo "<th>Teléfono</th>";
                     echo "<th>Email</th>";
                     echo "<th>Website</th>";
                 echo "</tr>";
             echo "</thead>";
             echo "<tbody>";
             while($row_monu = mysqli_fetch_array($result_monu)){
                 echo "<tr>";
                     echo "<td>" . $row_monu['nombre'] . "</td>";
                     echo "<td>" . $row_monu['direccion'] . "</td>";
                     echo "<td>" . $row_monu['telefono'] . "</td>";
                     echo "<td>" . $row_monu['email'] . "</td>";
                     echo "<td>" . $row_monu['website'] . "</td>";
                 echo "</tr>";
             }
             echo "</tbody>";                            
         echo "</table>";
         mysqli_free_result($result_monu);
     } else{    
     }
 } else{
 }
// ------- termina buscar museos
 //----------- Buscar Teatros  ----------
 $sql_monu = "SELECT * FROM teatros WHERE cod_loc LIKE '%"."$loca"."%'";
 if($result_monu = mysqli_query($link, $sql_monu)){
     if(mysqli_num_rows($result_monu) > 0){
         echo "<br><h3>Teatros</h3>";
         echo '<table class="table table-bordered table-dark">';
             echo "<thead>";
                 echo "<tr>";
                     echo "<th>Nombre</th>";
                     echo "<th>Dirección</th>";
                     echo "<th>Capacidad</th>";
                     echo "<th>Teléfono</th>";
                     echo "<th>Email</th>";
                     echo "<th>Website</th>";
                 echo "</tr>";
             echo "</thead>";
             echo "<tbody>";
             while($row_monu = mysqli_fetch_array($result_monu)){
                 echo "<tr>";
                     echo "<td>" . $row_monu['nombre'] . "</td>";
                     echo "<td>" . $row_monu['direccion'] . "</td>";
                     echo "<td>" . $row_monu['capacidad'] . "</td>";
                     echo "<td>" . $row_monu['telefono'] . "</td>";
                     echo "<td>" . $row_monu['email'] . "</td>";
                     echo "<td>" . $row_monu['website'] . "</td>";
                 echo "</tr>";
             }
             echo "</tbody>";                            
         echo "</table>";
         mysqli_free_result($result_monu);
     } else{    
     }
 } else{
 }
// ------- termina buscar teatros

}
                    ?>
   


   </div>
            </div>
        </div>
    </body>
</html>
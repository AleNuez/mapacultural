# MapaCultural

V1.0.1

http://alenuez38.atwebpages.com/site/MapaCultural/www/app.php

## Name
Buscador de sitios de interés cultural usando datos del gobierno nacional.

## Description
Proyecto realizado en PHP.
Consta de un seleccionador de provincias que muestra los departamentos que contiene.
Una vez seleccionado el departamento se pueden ver las distintas ofertas culturales que el lugar ofrece.
Estas pueden ser:
- Museos
- Cines
- Sitios Històricos
- Teatros

## Installation
-Asegurarse que Docker esté corriendo.
-Crear BD "MapaCultural"
-Importar BD MapaCultural-dump.sql
-Ejecutar shell.sh -> bash shell.sh (desde windows ejecutar bash shell-win.sh)
## Support
Contactar alejandro.emmanuel.n@gmail.com

## Project status
- Repositorio : OK
- Base de datos: OK
- Dashboard: OK
- Bugs: En curso

- Consideraciones 
1. En los diferentes datasets las comunas de CABA no estàn detalladas, todas las comunas estàn bajo el mismo ID por lo que si uno busca las ofertas en Ciudad de Buenos Aires se mostraràn todas las ofertas sin diferenciar barrios porteños.
